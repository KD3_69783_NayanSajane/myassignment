import React from 'react'
import Counter from './components/Counter'
import {useDispatch} from 'react-redux';

function ReduxDemo() {
const dispatch = useDispatch();

  return (
    <div>
      <button onClick={e=> dispatch({type:'INCREMENT'})} > Increment </button>
        <Counter></Counter>
      <button onClick={e=> dispatch({type: 'DECREMENT'})}> Decrement </button>
    </div>
  )
}

export default ReduxDemo


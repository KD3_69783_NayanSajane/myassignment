import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import AxiosTutorial from './axiosdemo/AxiosTutorial';
import AxiosPost from './axiosdemo/AxiosPost';
import AxiosThapa from './axiosdemo/AxiosThapa';
// import App from './App';
// import ReduxDemo from './ReduxDemo';
// import {Provider} from 'react-redux';
// import {store} from './Redux/Store';


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    {/* <Provider store={store}>
    <ReduxDemo/>
    <App />
    </Provider> */}
    {/* <AxiosTutorial></AxiosTutorial> */}
    {/* <AxiosPost/> */}
    <AxiosThapa/>
  </React.StrictMode>
);


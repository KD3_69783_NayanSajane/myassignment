import React, {useEffect, useState} from 'react'
import axios from 'axios';
import './AxiosThapa.css'

function AxiosThapa() {

    const [mydata, setMydata] = useState([]);
    const [isError, setIsError] = useState("");
    const API = "https://jsonplaceholder.typicode.com";

    // useEffect(() => {
    //     axios.get("https://jsonplaceholder.typicode.com/posts")
    //     .then((res)=>
    //     //console.log(res.data)
    //     setMydata(res.data))
    //     .catch((error)=> setIsError(error.message));
    // }, []);

    // async 
    useEffect(() => {
        getApiData(`${API}/posts`);
    }, []);

    const getApiData = async(url) => {
    try {
        const res = await axios.get(url);
        setMydata(res.data);
    } catch (error) {
        setIsError(error.message);
    }
}

  return (
    <>
      <h1> Axios Tutorial </h1>
      {isError !== "" && <h2> {isError}</h2>}
      <div className="grid container my-3 mx-3">
      {mydata.map((post)=>{
        const {id, title, body} = post;
        return(
            <div className="card" key={id}>
                <h2>{title}</h2>
                <p>{body}</p>
            </div>
        )
      })}
      </div>
    </>
  )
}

export default AxiosThapa

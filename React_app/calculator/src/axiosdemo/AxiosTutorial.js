import React, { useState, useEffect } from 'react';
import axios from 'axios';

function AxiosTutorial() {

   const [userData, setUserData] = useState([]);
  
  useEffect(()=>{
    axios.get("https://jsonplaceholder.typicode.com/users")
    .then((res)=>{
      console.log(res)
      setUserData(res.data)
    })
  },[])

  return (
    <div>
     {userData.map((data)=>{
      return(
        <center>{data.name}</center>
      )
     })}
    </div>
  )
}

export default AxiosTutorial

import axios from 'axios';
import React, { useState, useEffect } from 'react';

function AxiosPost() {
    const data = {
        fname: '',
        lastName: ''
    };

    const [inputData, setInputData] = useState(data);

    const handleData=(e)=>{
        setInputData({...inputData, [e.target.name]: e.target.value})
    }

    const handleSubmit = (e)=>{
       e.preventDefault(); 
       axios.post("https://jsonplaceholder.typicode.com/users", inputData)
       .then((res)=>{
        console.log(res)
       })
    }

    const handleUpdate =(e)=>{
        e.preventDefault();
        axios.put("https://jsonplaceholder.typicode.com/users/1", inputData)
        .then((res)=>{
            console.log(res)
        })
    }

    const handleDelete=(e)=>{
        e.preventDefault();
        axios.delete("https://jsonplaceholder.typicode.com/users/1")
        .then((res)=>{
            console.log(res)
        })
    }

  return (
    <>
      <label>First Name :</label>
      <input type='text' name='fname' value={inputData.fname} onChange={handleData}></input><br/>

      <label>Last Name :</label>
      <input type='text' name='lastName' value={inputData.lastName} onChange={handleData}></input><br/>

      <button onClick={handleSubmit}> Submit </button> <br></br>
      <button onClick={handleUpdate}> Update </button>
      <button onClick={handleDelete}> Delete </button>
    </>
  )
}

export default AxiosPost

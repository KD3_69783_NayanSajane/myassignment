import React, { useState } from 'react';
import './Registration.css';
import { Link } from 'react-router-dom';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';

const Registration = (props) => {

  const navigate = useNavigate();

  const [formData, setFormData] = useState({
    firstName: '',
    lastName: '',
    email: '',
    password: '',
    contactNo: '',
  });

  const [errors, setErrors] = useState({
    firstName: '',
    lastName: '',
    password: '',
    contactNo: '',
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
    
    let error = '';

    switch (name) {
      case 'firstName':
        error = value.length < 5 || value.length > 20 ? 'First name should be between 5 and 20 characters' : '';
        break;
      case 'lastName':
        error = value.length < 5 || value.length > 20 ? 'Last name should be between 5 and 20 characters' : '';
        break;
      case 'password':
        error = value.length < 6 ? 'Password should be at least 6 characters long' : '';
        break;
      case 'contactNo':
        error = !/^\d{10}$/.test(value) ? 'Invalid contact number' : '';
        break;
      default:
        break;
    }

    setErrors({
      ...errors,
      [name]: error,
    });

    setFormData({
      ...formData,
      [name]: value,
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    axios.post("http://localhost:7070/user/register", formData)
    .then((res)=>{
      console.log("Form submitted: " , res.data);
      navigate("/login");
      props.showAlert(res.data)
    })
    .catch((err)=>{
      console.error("Registration Failed : ", err);
    })

    setFormData({
      firstName: '',
      lastName: '',
      email: '',
      password: '',
      contactNo: '',
    });
  };

  return (
    <div className="registration-container my-3">
      <center>
        <h2>Registration</h2>
      </center>
      <form onSubmit={handleSubmit}>
        <div className="form-group">
          <label>First Name:</label>
          <input
            type="text"
            name="firstName"
            value={formData.firstName}
            onChange={handleChange}
            required
          />
          {errors.firstName && <p className="error">{errors.firstName}</p>}
        </div>
        <div className="form-group">
          <label>Last Name:</label>
          <input
            type="text"
            name="lastName"
            value={formData.lastName}
            onChange={handleChange}
            required
          />
          {errors.lastName && <p className="error">{errors.lastName}</p>}
        </div>
        <div className="form-group">
          <label>Email:</label>
          <input
            type="email"
            name="email"
            value={formData.email}
            onChange={handleChange}
            required
          />
        </div>
        <div className="form-group">
          <label>Password:</label>
          <input
            type="password"
            name="password"
            value={formData.password}
            onChange={handleChange}
            required
          />
          {errors.password && <p className="error">{errors.password}</p>}
        </div>
        <div className="form-group">
          <label>Contact Number:</label>
          <input
            type="tel"
            name="contactNo"
            value={formData.contactNo}
            onChange={handleChange}
            required
          />
          {errors.contactNo && <p className="error">{errors.contactNo}</p>}
        </div>
        <center>
          <button type="submit">Register</button>
        </center>
      </form>
      <center>
        Already have an account? <Link to="/login">Login here!</Link>
      </center>
    </div>
  );
};

export default Registration;

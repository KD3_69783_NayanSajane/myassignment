import { FETCH_PRODUCTS, DELETE_PRODUCT, ADD_PRODUCT, UPDATE_PRODUCT } from "../actions/ProductActions";

const initialState ={
    products: [], 
};

const ProductReducer = (state= initialState, action) => {
  switch(action.type){
    case FETCH_PRODUCTS : 
    return{
        ...state,
        products: action.payload,
    };
    case DELETE_PRODUCT:
    return{
        ...state,
        products: state.products.filter((product)=> product.id !== action.payload),
    } 
   case ADD_PRODUCT:
    return{
        ...state,
        products: [...state.products, action.payload],
    };
    case UPDATE_PRODUCT:
      return {
        ...state,
        products: state.products.map((product) =>
          product.id === action.payload.id ? action.payload : product
        ),
      };
    default:
        return state;
  }
}

export default ProductReducer

import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import '../login/Login.css';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';

const Login = (props) => {
  const navigate = useNavigate();
  const [formData, setFormData] = useState({
    email: '',
    password: '',
  });

  const [error, setError] = useState(null);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({
      ...formData,
      [name]: value,
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    axios.post("http://localhost:7070/user/login", formData)
      .then((res) => {
        console.log('Form submitted:', res.data.firstName);
        navigate("/home");
        props.showAlert("LogIn", "U logged In successfully")
      })
      .catch((err) => {
        console.error('Login failed:', err);
        setError("Wrong email or password.");
      });
  };

  return (
    <div className="login-container my-3">
      <center><h2>Login</h2></center>
      <form onSubmit={handleSubmit}>
        <div className="form-group">
          <label>Email:</label>
          <input
            type="email"
            name="email"
            value={formData.email}
            onChange={handleChange}
            required
          />
        </div>
        <div className="form-group">
          <label>Password:</label>
          <input
            type="password"
            name="password"
            value={formData.password}
            onChange={handleChange}
            required
          />
        </div>
        <center><button type="submit"> Login </button></center>
        {error && <p style={{backgroundColor:"red", color:"white"}}><center>{error}</center></p>}
      </form>
      <center>
        Don't have an account? <Link to="/register">Sign up</Link>
      </center>
    </div>
  );
};

export default Login;

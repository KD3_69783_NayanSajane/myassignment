import React from 'react'

function Alert(props) {
  return (
    <div style={{ float: 'right', height: '3px' }}>
      {props.alert && (
        <div className="alert alert-success alert-dismissible fade show" role="alert" style={{ width: '400px' }}>
          <strong>{props.alert.type}</strong>  {props.alert.msg}
          <button type="button" className="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
      )}
    </div>
  );
}




export default Alert
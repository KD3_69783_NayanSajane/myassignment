import React from "react";

function About() {
	return (
		<div className="container my-3" >
			<h2>
				This is the product management Application.
			</h2>
			Read more about us at :
			<a href="http://localhost:3000/home">
			Product Management System
			</a>
		</div>
	);
}
export default About;

import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { deleteProduct, fetchProducts } from '../actions/ProductActions';

function ProductList() {
  const dispatch = useDispatch();
  const products = useSelector((state) => state.products);

  useEffect(() => {
    dispatch(fetchProducts());  // action call
  }, [dispatch]);

  const handleDeleteProduct = (id) =>{
    dispatch(deleteProduct(id));
  }

  return (
    <div>
      <h1>Product List</h1>
      <button>Add Product</button>
      <table className="table">
        <thead>
          <tr>
            <th>ID</th>
            <th>Operator</th>
            <th>Product Name</th>
            <th>Cost</th>
            <th>Color</th>
            <th>Storage (GB)</th>
            <th>RAM (GB)</th>
            <th>Rating</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {products.map((product) => (
            <tr key={product.id}>
              <td>{product.id}</td>
              <td>{product.operator}</td>
              <td>{product.productName}</td>
              <td> Rs. {product.cost} /-</td>
              <td>{product.color}</td>
              <td>{product.storage} </td>
              <td>{product.ram}</td>
              <td>{product.rating}</td>
              <td>
                <button type="button" className="btn btn-danger" onClick={() => handleDeleteProduct(product.id)}>
                  Delete
                </button>{' '}
                <button type="button" className="btn btn-primary">
                  Update
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default ProductList;

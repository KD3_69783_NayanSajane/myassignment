import React from 'react'
import {Row, Col } from 'react-bootstrap';
import './Footer.css'

const Footer = () => {
  return (
    <footer className="bg-light mt-auto" >
        <div className="main-footer">
            <div className="container">        
                <Row>
                <Col lg={4} md={4}>
                    <h5><b>About Us</b></h5>
                    <p>
                   This is Employee management application.
                    </p>
                </Col>
                <Col lg={4} md={4}>
                    <h5><b>Contact Information</b></h5>
                    <p>Email: nayansajane@gmail.com</p>
                    <p>Phone: (+91) 8605074889 </p>
                </Col>
                <Col lg={4} md={6}>
                    <h5><b>Follow Us</b></h5>
                    <p>
                    Connect with us on social media for updates and news.
                    </p>
                   
                    <p>Facebook | Twitter | LinkedIn</p>
                </Col>
                </Row>
      
                <div className="bg-dark text-light text-center py-1">
                    <p>&copy; 2023 Your Company. All rights reserved.</p>
                </div>
             </div>
        </div>
    </footer>
  );
};

export default Footer
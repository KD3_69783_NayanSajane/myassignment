import React from "react";
 
function Contact() {
    
    return (
        <address className="container my-3">
                You can find us here:
            <br />
                Product Management
            <br />
            <h5><b>Contact Information</b></h5>
                <p>Email: nayansajane@gmail.com</p>
                <p>Phone: (+91) 8605074889 </p>
        </address>
    );
}
 
export default Contact;
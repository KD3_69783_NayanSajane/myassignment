import React from 'react';
import { Link } from 'react-router-dom';
import { useNavigate } from "react-router-dom";


function Navigation() {

  const navigate = useNavigate();

  const handleLogOut = () =>{
    navigate("/register");
  }

  const handleLogIn = ()=>{
    navigate("/login");
  }

  return (
    <div >
      <header className="p-2 text-bg-dark">
        
          <div className="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">
            <Link to="/" className="d-flex align-items-center mb-2 mb-lg-0 text-white text-decoration-none">My App
              <svg className="bi me-2" width="40" height="32" role="img" aria-label="Bootstrap">
                <use xlinkto="#bootstrap" />
              </svg>
            </Link>

            <ul className="nav col-12 col-lg-auto me-lg-auto mb-2 justify-content-center mb-md-0">
              <li><Link to="/contact" className="nav-link px-2 text-white">Contact Us</Link></li>
              <li><Link to="/faqs" className="nav-link px-2 text-white">FAQs</Link></li>
              <li><Link to="/about" className="nav-link px-2 text-white">About</Link></li>
              <li><Link to="/calci" className="nav-link px-2 text-white">Calculator</Link></li>
            </ul>
            
            
            {/* <div class="form-check form-switch">
                <input className="form-check-input" type="checkbox" role="switch" id="flexSwitchCheckDefault"/>
                <label className="form-check-label" htmlFor="flexSwitchCheckDefault"> Toggle</label>
            </div>
            <div className="d-flex">
                <div className="bg-primary rounded mx-2" style={{height:'30px', width:'30px'}}></div>
            </div>  */}

            <div className="text-end">          
              <button type="button" className="btn btn-outline-light me-2" onClick={handleLogIn}>Login</button>
              <button type="button" className="btn btn-warning" onClick={handleLogOut}>Sign-up</button>
            </div>
            
          </div>
        
      </header>
    </div>
  );
}

export default Navigation;

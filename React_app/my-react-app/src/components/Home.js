import React from "react";
import ProductList from "../products/ProductList";
 
function Home() {
   
    return(
       <div className="container my-3">        
        <ProductList/>
        </div>
    ) 
}
export default Home;
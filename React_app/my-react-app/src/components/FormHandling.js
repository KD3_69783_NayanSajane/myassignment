import React, { Component } from 'react'

class FormHandling extends Component {
    constructor(props) {
      super(props)
    
      this.state = {
         username: '',
         comments: '',
         topic:'Java'
      }
      this.handleUsernameChange = this.handleUsernameChange.bind(this);
    }

    handleUsernameChange(event){
        this.setState({username: event.target.value})
    }

    // handleUsernameChange =(event) =>{
    //     this.setState({username: event.target.value})
    // }

    // handleCommentsChange = (e) =>{
    //     this.setState({comments: e.target.value})
    // }

    handleTopicChange = (event ) =>{
        this.setState({topic: event.target.value})
    }

    handleSubmit = (event) =>{
        alert("submitted succesfully" + `${this.state.comments}`);
        event.preventDefault();
    }
    
  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <center>
            <label>Username :</label>
            <input type="text" 
            value={this.state.username}
            onChange={this.handleUsernameChange}/>
        </center>
        <center>
            <label>Comments</label>
            <textarea value={this.state.comments}
            onChange={(e)=>this.setState({comments: e.target.value})}>
            </textarea>
        </center>
        <center>
            <label>Topic</label>
            <select value= {this.state.topic}
            onChange={this.handleTopicChange}>
                <option value="React"> React </option>
                <option value="Angular"> Angular </option>
                <option value="Java"> Java </option>
            </select><br></br>
            <button type='submit'>Submit</button>
        </center>
       
      </form>
    )
  }
}

export default FormHandling
import React, { useState } from 'react'
import Display from "../components/Display";
import ButtonsContainer from "../components/ButtonContainer";
import "./Calculator.css";

function Calculator() {
    const [calVal, setCalVal] = useState("");

    const onButtonClick = (buttonText) => {
      if (buttonText === "C") {
        setCalVal("");
      } else if (buttonText === "=") {
        const result = eval(calVal);
        setCalVal(result);
      } else {
        const newDisplayValue = calVal + buttonText;
        setCalVal(newDisplayValue);
      }
    };
  
    return (
      <div className="calculator container my-3">
        <Display displayValue={calVal}></Display>
        <ButtonsContainer onButtonClick={onButtonClick}></ButtonsContainer>
      </div>
    );
}

export default Calculator

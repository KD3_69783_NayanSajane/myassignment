import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { deleteProduct, fetchProducts } from '../actions/ProductActions';
import { useNavigate } from "react-router-dom";
import Swal from 'sweetalert2';

function ProductList() {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const products = useSelector((state) => state.products);

  useEffect(() => {
    console.log('Fetching products...');
    dispatch(fetchProducts());  // action call
  }, [fetchProducts]);

  const handleDeleteProduct = (id) =>{
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: "btn btn-success",
        cancelButton: "btn btn-danger"
      },
      buttonsStyling: false
    });
    swalWithBootstrapButtons.fire({
      title: "Are you sure?",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonText: "Yes, delete it!",
      cancelButtonText: "No, cancel!",
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        dispatch(deleteProduct(id));
        swalWithBootstrapButtons.fire({
          title: "Deleted!",
          text: "Your Product has been deleted.",
          icon: "success"
        });
      } else if (
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire({
          title: "Cancelled",
          text: "Your product is safe :)",
          icon: "error"
        });
      }
    });
    
  }

  const handleAddProduct =()=>{
    navigate('./addProduct')
  }

  const handleUpdateProduct=(id)=>{
    navigate(`./updateProduct/${id}`);
  }

  return (
    <div>
      <h1>Product List</h1>
      <button type="button" onClick={handleAddProduct} >Add Product</button>
      <table className="table">
        <thead>
          <tr>
            <th>ID</th>
            <th>Operator</th>
            <th>Product Name</th>
            <th>Cost</th>
            <th>Color</th>
            <th>Storage (GB)</th>
            <th>RAM (GB)</th>
            <th>Rating (5) </th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {products.map((product) => (
            <tr key={product.id}>
              <td>{product.id}</td>
              <td>{product.operator}</td>
              <td>{product.productName}</td>
              <td> ₹ {product.cost} /-</td>
              <td>{product.color}</td>
              <td>{product.storage} </td>
              <td>{product.ram}</td>
              <td>{product.rating}</td>
              <td>
                <button type="button" className="btn btn-danger" onClick={() => handleDeleteProduct(product.id)}>
                  Delete
                </button>{' '}
                <button type="button" className="btn btn-primary" onClick={()=> handleUpdateProduct(product.id)}>
                  Update
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
      <button type="button" onClick={handleAddProduct} >Add Product</button>
    </div>
  );
}

export default ProductList;

import React, {useState}  from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from "react-router-dom";
import { useParams } from 'react-router-dom';
import { updateProducts } from '../actions/ProductActions';
import Swal from 'sweetalert2';

const UpdateProduct = (props) => {

    const { id } = useParams();
    const productId = parseInt(id);
    const navigate = useNavigate();
    const dispatch = useDispatch() ;
    const product = useSelector((state) => state.products.find((product) => product.id === productId));
    
    const [updatedProduct, setUpdatedProduct] = useState({
        id: productId,
        color:product.color,
        cost:product.cost,
        operator:product.operator,
        productName:product.productName,
        ram:product.ram,
        rating:product.rating,
        storage:product.storage,
      });

      const handleInputChange = (e) => {
        const { name, value } = e.target;
        setUpdatedProduct((prevProduct) => ({
          ...prevProduct,
          [name]: value,
        }));
      };

      const handleUpdateProduct = (e)=>{
        e.preventDefault();
        console.log("inside handleUpdateProduct")
        dispatch(updateProducts(updatedProduct));
        Swal.fire({
            position: "top-end",
            icon: "success",
            title: "ID: " +`${updatedProduct.id}` + " Updated and saved Successfully",
            showConfirmButton: false,
            timer: 4000
          });
        setUpdatedProduct({
            color:'',
            cost:'',
            operator:'',
            productName:'',
            ram:'',
            rating:'',
            storage:'',
        })
        navigate('/home')
      }

  return (
    <div className="container my-3">
    <form className="registration-container my-3" >
    <center>
       <i> <h2 style={{ color: 'gray' }}>Update the required details</h2></i>
      </center>
      <div className="row">
        <div className="col-md-6">
          <label>
            <b>Product Name :</b>
            <input
              type="text"
              name="productName"
              value={updatedProduct.productName}
              onChange={handleInputChange}
              required
            />
          </label>
        </div>
        <div className="col-md-6">
          <label>
          <b>Operator: </b>
            <input
              type="text"
              name="operator"
              value={updatedProduct.operator}
              onChange={handleInputChange}
              required
            />
          </label>
        </div>
      </div>
      <div className="row">
        <div className="col-md-6">
          <label>
          <b>Cost:</b>
            <input
              type="text"
              name="cost"
              value={updatedProduct.cost}
              onChange={handleInputChange}
              required
            />
          </label>
        </div>
        <div className="col-md-6">
          <label>
          <b>Color:</b>
            <input
              type="text"
              name="color"
              value={updatedProduct.color}
              onChange={handleInputChange}
              required
            />
          </label>
        </div>
      </div>
      <div className="row">
        <div className="col-md-6">
          <label>
          <b> RAM:</b>
            <input
              type="text"
              name="ram"
              value={updatedProduct.ram}
              onChange={handleInputChange}
              required
            />
          </label>
        </div>
        <div className="col-md-6">
          <label>
          <b> Rating:</b>
            <input
              type="text"
              name="rating"
              value={updatedProduct.rating}
              onChange={handleInputChange}
              required
            />
          </label>
        </div>
        <div className="col-md-6">
          <label>
          <b> Storage:</b>
            <input
              type="text"
              name="storage"
              value={updatedProduct.storage}
              onChange={handleInputChange}
              required
            />
          </label>
        </div>
      </div>
      <center>
      <button className="btn btn-primary" type="submit" onClick={handleUpdateProduct}> Update Product </button>
      </center>
    </form>
  </div>
  )
}

export default UpdateProduct

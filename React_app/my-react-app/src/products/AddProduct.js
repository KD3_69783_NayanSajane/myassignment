import React, {useState}  from 'react'
import { useDispatch } from 'react-redux';
import { useNavigate } from "react-router-dom";
import { addProducts } from '../actions/ProductActions';
import Swal from 'sweetalert2';

const AddProduct = (props) => {
  const navigate = useNavigate();
  const dispatch = useDispatch() ;

  const [newProduct, setNewProduct] = useState({
    color:'',
    cost:'',
    operator:'',
    productName:'',
    ram:'',
    rating:'',
    storage:'',
  });

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setNewProduct((prevProduct) => ({
      ...prevProduct,
      [name]: value,
    }));
  };

  const handleAddProduct = (e) => {
    e.preventDefault();
    console.log("inside handleAddProduct");
    Swal.fire({
      title: "Do you want to add new the product?",
      showDenyButton: true,
      showCancelButton: true,
      confirmButtonText: "Save",
      denyButtonText: `Don't save`
    }).then((result) => {
      if (result.isConfirmed) {
        dispatch(addProducts(newProduct));
        Swal.fire("Saved!", "", "success");
        setNewProduct({
          color: "",
          cost: "",
          operator: "",
          productName: "",
          ram: "",
          rating: "",
          storage: "",
        });
        navigate("/home");
      } else if (result.isDenied) {
        Swal.fire("Changes are not saved", "", "info");
      }
    });
  };
  

  return (
    <div className="container my-3">
    <form className="registration-container my-3" >
    <center>
    <i> <h2 style={{ color: 'gray' }}>Fill the Details</h2></i>
      </center>
      <div className="row">
        <div className="col-md-6">
          <label>
            <b>Product Name :</b>
            <input
              type="text"
              name="productName"
              value={newProduct.productName}
              onChange={handleInputChange}
              required
            />
          </label>
        </div>
        <div className="col-md-6">
          <label>
          <b>Operator: </b>
            <input
              type="text"
              name="operator"
              value={newProduct.operator}
              onChange={handleInputChange}
              required
            />
          </label>
        </div>
      </div>
      <div className="row">
        <div className="col-md-6">
          <label>
          <b>Cost:</b>
            <input
              type="text"
              name="cost"
              value={newProduct.cost}
              onChange={handleInputChange}
              required
            />
          </label>
        </div>
        <div className="col-md-6">
          <label>
          <b>Color:</b>
            <input
              type="text"
              name="color"
              value={newProduct.color}
              onChange={handleInputChange}
              required
            />
          </label>
        </div>
      </div>
      <div className="row">
        <div className="col-md-6">
          <label>
          <b> RAM: in GB</b>
            <input
              type="text"
              name="ram"
              value={newProduct.ram}
              onChange={handleInputChange}
              required
            />
          </label>
        </div>
        <div className="col-md-6">
          <label>
          <b> Rating: Out of 5</b>
            <input
              type="text"
              name="rating"
              value={newProduct.rating}
              onChange={handleInputChange}
              required
            />
          </label>
        </div>
        <div className="col-md-6">
          <label>
          <b> Storage:</b>
            <input
              type="text"
              name="storage"
              value={newProduct.storage}
              onChange={handleInputChange}
              required
            />
          </label>
        </div>
      </div>
      <center>
      <button type="submit" onClick={handleAddProduct}> Add Product </button>
      </center>
    </form>
  </div>
  
  )
}

export default AddProduct


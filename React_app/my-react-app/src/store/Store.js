import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk'; // Import 'thunk' correctly
import ProductReducer from '../reducers/ProductReducer';

const store = createStore(ProductReducer, applyMiddleware(thunk));

export default store;
import FormHandling from "./components/FormHandling";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Contact from "./components/Contact";
import Home from "./components/Home";
import About from "./components/About";
import Navigation from './components/Navigation';
import Footer from "./components/Footer";
import './App.css';
import Alert from "./components/Alert";
import {useState} from 'react';
import Calculator from './components/Calculator'
import Sidebar from "./components/Sidebar";
import Login from "./login/Login";
import Registration from "./registration/Registration";
import { Provider } from "react-redux";
import store from "./store/Store";
import AddProduct from "./products/AddProduct";
import UpdateProduct from "./products/UpdateProduct";

function App() {
  const [alert, setAlert] = useState(null)
  const [user, setUser] = useState("Logged In");

  const showAlert = (message, type) =>{
    setAlert({
      msg:message,
      type:type
    })
    setTimeout(() => {
      setAlert(null);
    }, 4000);
  }

  return (
   <div className="page-container">
    <Provider store={store}>
    <div className="content-wrap">
     <BrowserRouter>
     <Navigation ></Navigation>
     <Alert alert={alert}/>
     {/* <Sidebar></Sidebar> */}
      <Routes>       
          <Route path="/" element={<Login showAlert={showAlert}/>} /> 
          <Route exact path="/home" element={<Home />} />
          <Route exact path="/contact" element={<Contact />} />  
          <Route exact path="/form" element={<FormHandling />} />  
          <Route exact path="/about" element={<About />} />
          <Route exact path="/calci" element={<Calculator />} /> 
          <Route exact path="/home/addProduct" element={<AddProduct showAlert={showAlert} />} /> 
          <Route exact path="/home/updateProduct/:id" element={<UpdateProduct showAlert={showAlert} />} /> 
          <Route exact path="/login" element={<Login showAlert={showAlert}/>} /> 
          <Route exact path="/register" element={<Registration showAlert={showAlert}/>} /> 
      </Routes>
      </BrowserRouter>
      </div>
      <Footer ></Footer> 
      </Provider>
   </div>
  );
}

export default App;

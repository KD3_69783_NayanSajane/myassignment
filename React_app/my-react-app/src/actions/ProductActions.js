import axios from 'axios';

export const FETCH_PRODUCTS = 'FETCH_PRODUCTS';
export const DELETE_PRODUCT = 'DELETE_PRODUCT';
export const ADD_PRODUCT = 'ADD_PRODUCT';
export const UPDATE_PRODUCT = 'UPDATE_PRODUCT';

export const fetchProducts = () => async (dispatch) => {
    try {
      console.log("inside axios fetch")
      const res = await axios.get('http://localhost:7070/user/getproducts');
      dispatch({
        type: FETCH_PRODUCTS, 
        payload: res.data,
      });
    } catch (error) {
      console.error('Error fetching products:', error.message);
    }
  };

export const deleteProduct = (id) => async(dispatch)=>{
    try{
        console.log("inside axios delete")
        await axios.delete(`http://localhost:7070/product/delete/${id}`);
        dispatch({
            type: DELETE_PRODUCT,
            payload: id,
        });   // will disatch or call  the reducer.
    }catch(error){
        console.log('Error during deleting product: ', error.message)
    }
};

export const addProducts =(newProduct) => async(dispatch)=>{
    try{
      console.log("inside axios add")
        const res = await axios.post('http://localhost:7070/product/add', newProduct);
        dispatch({
            type: ADD_PRODUCT,
            payload: res.data,
        });
    }catch(error){
        console.error("Error during adding product: ", error.message);
    }
}

export const updateProducts = (updatedProduct) => async (dispatch) => {
    try {
      console.log("inside axios update")
      const response = await axios.put(`http://localhost:7070/product/update/${updatedProduct.id}`, updatedProduct);
      dispatch({
        type: UPDATE_PRODUCT,
        payload: response.data,
      });
    } catch (error) {
      console.error('Error updating product:', error.message);
    }
  };


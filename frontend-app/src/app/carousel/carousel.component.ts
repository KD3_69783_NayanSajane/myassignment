import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.css'],
  
})

export class CarouselComponent implements OnInit{
  
  constructor() { }

  ngOnInit(): void {
  }
  
  images: string[] = [
    'https://placekitten.com/800/400',
    'https://placekitten.com/800/401',
    'https://placekitten.com/800/402',
    'https://placekitten.com/800/403',
    'https://placekitten.com/800/404',
  ];
  currentImageIndex: number = 0;

  showPreviousImage() {
    this.currentImageIndex = (this.currentImageIndex - 1 + this.images.length) % this.images.length;
  }

  showNextImage() {
    this.currentImageIndex = (this.currentImageIndex + 1) % this.images.length;
  }

}

import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ProductServiceService } from '../productservice.service';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit{
  productForm: any;
  

  constructor(private fb: FormBuilder, private service: ProductServiceService){
    this.productForm = this.fb.group({
      ProductName:[""],
      Cost:[""],
      Color:[""],
      Storage:[""],
      Ram:[""],
      Operator:[""],
      Rating:[""]
      
    })
  }
  ngOnInit(): void {
   
  }

  AddProduct(value:any){
    let body = {
      productName: value.ProductName,
      cost: value.Cost,
      color: value.Color,
      storage: value.Storage,
      ram: value.Ram,
      operator: value.Operator,
      rating: value.Rating

    }
    console.log("Component:", body);
    this.service.addProductDetails(body).subscribe(data=>{
      alert("data added");
      console.log(data);
    })
  }

  
}

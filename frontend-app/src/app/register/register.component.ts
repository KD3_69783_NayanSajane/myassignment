import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {

  uname: string = "";
  uemail: string = "";
  upassword: string = "";

constructor(private http: HttpClient)
{

}

save()
{
  console.log("Hello Nayan");
  let bodyData = {
    "name" : this.uname,
    "email" : this.uemail,
    "password" : this.upassword 
  };

  this.http.post("http://localhost:7070/user/register", bodyData, {responseType: 'text'}).subscribe((resultData: any)=>
  {
    console.log(resultData);
    alert("Employee registered successfully !!");
  });
}

}

import { Component } from '@angular/core';
import { ProductServiceService } from '../productservice.service';

@Component({
  selector: 'app-view-product',
  templateUrl: './view-product.component.html',
  styleUrls: ['./view-product.component.css']
})
export class ViewProductComponent {
  
  products:any;
  constructor(private productData: ProductServiceService)
  {
    this.productData.getProductDetails().subscribe((data)=>{
      this.products=data;
      console.log("In component ", this.products);
    })
  }

  deleteProductById(product: any){
    console.log("In component delete :",product.id);
    this.productData.deleteProductById(product.id).subscribe((resp)=>{
        console.log(resp);
        alert("Product Deleted")
        this.productData.getProductDetails();
      },
      err=>{
        console.log(err);
      }
    );
     
  }
}

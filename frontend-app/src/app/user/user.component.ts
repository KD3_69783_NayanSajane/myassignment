import { Component } from '@angular/core';
import { UserServiceService } from '../user-service.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent {

  users:any;
  constructor(private userData: UserServiceService)
  {
    this.userData.getUserDetails().subscribe((data)=>{
      this.users=data;
      console.log("In component ", this.users);
    })
  }

}

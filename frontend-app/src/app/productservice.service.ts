import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductServiceService {

  readonly url = "http://localhost:7070";
  constructor(private http:HttpClient) {
    
  }

  addProductDetails(data: any):Observable<any>{
    console.log("Service:",data);
    return this.http.post(this.url+"/product/AddProd",data);
  }

  getProductDetails(){
    console.log(this.http.get(this.url+"/product/AllProducts"));
    return this.http.get(this.url+"/product/AllProducts");
  }

  deleteProductById(id: any){
    console.log("delete by id in service ",id);
    return this.http.delete(this.url+"/product/delete/"+id);
  }
  
  editProductDetailsById(data: any){
    console.log("in service edit :",data.id);
    return this.http.put(this.url+"/product/"+data.id,data );
  }

  
}

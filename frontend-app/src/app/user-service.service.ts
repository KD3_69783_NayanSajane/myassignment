import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserServiceService {

  readonly url = "http://localhost:7070";
  constructor(private http:HttpClient) {
    
  }

  getUserDetails(){
    console.log(this.http.get(this.url+"/user/allUsers"));
    return this.http.get(this.url+"/user/allUsers");
  }
}

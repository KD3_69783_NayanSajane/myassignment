import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  uemail: string = "";
  upassword: string = "";

  constructor(private http: HttpClient, private router: Router)
  {

  }

  login(){
    console.log(this.uemail);
    console.log(this.upassword);

    let bodyData = {
      email: this.uemail,
      password: this.upassword
    };

      this.http.post("http://localhost:7070/user/login", bodyData).subscribe((resultData: any)=>{
        console.log(resultData);

        if(resultData.email == this.uemail)
        {
          this.router.navigateByUrl('/carousel');
        }
        else if(resultData.email != this.uemail )
        {
          alert("User not Exist")
        }
        else
        {
          alert("Enter correct password!!")
        }
      }
      )
  }
}

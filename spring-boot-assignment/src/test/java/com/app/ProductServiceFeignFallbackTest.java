package com.app;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import com.app.pojos.PostOfficeResponseBean;
import com.app.service.IProductServiceFeign;
import com.app.service.ProductServiceFeignFallback;

@SpringBootTest
public class ProductServiceFeignFallbackTest 
{
    @InjectMocks
    private ProductServiceFeignFallback productServiceFeignFallback;

    @Mock
    private IProductServiceFeign productServiceFeign;

    @BeforeEach
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testFallback() {
        String city = "TestCity";

        PostOfficeResponseBean errorResponse = new PostOfficeResponseBean();
        errorResponse.setMessage("server down");

        Mockito.when(productServiceFeign.fetchPostOfficeDetailsFeignByCity(city))
        .thenReturn(new PostOfficeResponseBean[]{errorResponse});

        PostOfficeResponseBean[] result = productServiceFeignFallback.fetchPostOfficeDetailsFeignByCity(city);

        assertEquals(1, result.length);
        assertEquals("server down", result[0].getMessage());
    }
}

package com.app.service;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.app.dao.UserDao;
import com.app.dto.UserDTO;
import com.app.exception.AppException;
import com.app.pojos.Users;

@RunWith(MockitoJUnitRunner.class)
public class IUserServiceImplTest 
{
    @InjectMocks
    private IUserServiceImpl userService;

    @Mock
    private UserDao userDao;

    @Test
    public void testRegisterUser() throws AppException 
    {
        Users mockUser = new Users();
        mockUser.setEmail("test@example.com");

        when(userDao.findByEmail(anyString())).thenReturn(null);
        when(userDao.save(any(Users.class))).thenReturn(mockUser);

        Users result = userService.registerUser(mockUser);
        assertEquals("test@example.com", result.getEmail());
    }

    @Test
    public void testAuthenticateUser() throws AppException 
    {
        String email = "test@example.com";
        String password = "password123";
        Users mockUser = new Users();
        mockUser.setEmail(email);
        mockUser.setPassword(password);

        when(userDao.findByEmailAndPassword(email, password)).thenReturn(mockUser);

        UserDTO result = userService.authenticateUser(email, password);
        assertEquals(email, result.getEmail());
    }

    @Test
    public void testGetAllUsers() 
    {
        List<Users> mockUsersList = new ArrayList<>();
        mockUsersList.add(new Users());
        mockUsersList.add(new Users());

        when(userDao.findAll()).thenReturn(mockUsersList);

        List<Users> result = userService.getAllUsers();
        assertEquals(2, result.size());
    }
}


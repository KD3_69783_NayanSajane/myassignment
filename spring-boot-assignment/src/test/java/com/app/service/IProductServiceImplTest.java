package com.app.service;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.app.dao.ProductDao;
import com.app.exception.AppException;
import com.app.pojos.Product;
import com.app.repo.ProductRepo;


@RunWith(MockitoJUnitRunner.class)
public class IProductServiceImplTest 
{
    @InjectMocks
    private IProductServiceImpl productService;

    @Mock
    private ProductRepo productRepo;

    @Mock
    private ProductDao productDao;

    @Test
    public void testAddProduct() throws AppException 
    {
        Product mockProduct = new Product();
        mockProduct.setId(1);
        mockProduct.setColor("red");
        mockProduct.setCost(10000);
        mockProduct.setOperator("Samsung");
        mockProduct.setProductName("A52s");
        mockProduct.setRam(6);
        mockProduct.setStorage(128);
        mockProduct.setRating(4.3f);
        Mockito.when(productDao.saveProductByJdbcTamplate(any())).thenReturn(1);

        int result = productService.addProduct(mockProduct);

        assertEquals(1, result);
    }

    @Test
    public void testUpdateProductById() throws AppException 
    {
        Product mockProduct = new Product();
        when(productDao.updateProductByJdbcTemplate(any())).thenReturn(1);

        Product result = productService.updateProductById(mockProduct);

        assertEquals(mockProduct, result);
    }

    @Test
    public void testDeleteProductById() throws AppException 
    {
        int productId = 1;
        
        when(productDao.deleteProductByJdbcTemplate(productId)).thenReturn(1);

        String result = productService.deleteProductById(productId);

        assertEquals("Product with id " + productId + " deleted successsfully!!!", result);
    }

    @Test
    public void testFindProductById() 
    {
        int productId = 1;
        Product mockProduct = new Product();
        mockProduct.setId(1);
        mockProduct.setColor("red");
        mockProduct.setCost(10000);
        mockProduct.setOperator("Samsung");
        mockProduct.setProductName("A52s");
        mockProduct.setRam(6);
        mockProduct.setStorage(128);
        mockProduct.setRating(4.3f);
        
        when(productDao.getProductById(productId)).thenReturn(mockProduct);

        Product result = productService.findProductByid(productId);
        assertEquals(mockProduct, result);
    }

    @Test
    public void testFindAllProductList() 
    {
        List<Product> mockProductList = new ArrayList<>();
        when(productDao.getAllProductList()).thenReturn(mockProductList);

        List<Product> result = productService.findAllProdustList();

        assertEquals(mockProductList, result);
    }
}


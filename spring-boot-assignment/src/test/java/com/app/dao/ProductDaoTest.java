package com.app.dao;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.app.exception.AppException;
import com.app.pojos.Product;
import com.app.dao.ProductDao;

@RunWith(MockitoJUnitRunner.class)
public class ProductDaoTest 
{

    @InjectMocks
    private ProductDao productDao;

    @Mock
    private JdbcTemplate jdbcTemplate;

    @Test
    public void testSaveProductByJdbcTemplate() throws AppException 
    {
        Product mockProduct = new Product();
        mockProduct.setProductName("TestProduct");
        mockProduct.setCost(100.0f);
        mockProduct.setColor("Red");
        mockProduct.setStorage(256);
        mockProduct.setRam(8);
        mockProduct.setOperator("TestOperator");
        mockProduct.setRating(4.5f);

        when(jdbcTemplate.update(anyString(), anyString(), anyFloat(), 
                anyString(), anyInt(), anyInt(), anyString(), anyFloat())).thenReturn(Integer.valueOf(1));

        int result = productDao.saveProductByJdbcTamplate(mockProduct);

        assertEquals(1, result);
    }


    @Test
    public void testUpdateProductByJdbcTemplate() 
    {
        Product mockProduct = new Product();
        mockProduct.setProductName("TestProduct");
        mockProduct.setCost(100.0f);
        mockProduct.setColor("Red");
        mockProduct.setStorage(256);
        mockProduct.setRam(8);
        mockProduct.setOperator("TestOperator");
        mockProduct.setRating(4.5f);

        when(jdbcTemplate.update(anyString(), anyString(), anyFloat(), 
                anyString(), anyInt(), anyInt(), anyString(), anyFloat(), anyInt())).thenReturn(Integer.valueOf(1));

        int result = productDao.updateProductByJdbcTemplate(mockProduct);
        assertEquals(1, result);
    }

    @Test
    public void testDeleteProductByJdbcTemplate() 
    {
        int productId = 1;
        when(jdbcTemplate.update(anyString(), anyInt())).thenReturn(1);
        int result = productDao.deleteProductByJdbcTemplate(productId);
        assertEquals(1, result);
    }

    @Test
    public void testGetProductById() 
    {
        int productId = 1;
        Product mockProduct = new Product();
        mockProduct.setId(productId);

        when(jdbcTemplate.queryForObject(anyString(), any(RowMapper.class), anyInt())).thenReturn(mockProduct);
        Product result = productDao.getProductById(productId);
        assertEquals(productId, result.getId());
    }

    @Test
    public void testGetAllProductList() 
    {
        List<Product> mockProductList = new ArrayList<>();
        mockProductList.add(new Product());
        mockProductList.add(new Product());

        when(jdbcTemplate.query(anyString(), any(RowMapperImpl.class))).thenReturn(mockProductList);

        List<Product> result = productDao.getAllProductList();
        assertEquals(2, result.size());
    }
}


package com.app.controller;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.app.exception.AppException;
import com.app.pojos.Product;
import com.app.service.IProductService;

@RunWith(MockitoJUnitRunner.class)
public class ProductControllerTest 
{
    @InjectMocks
    private ProductController productController;

    @Mock
    private IProductService iProductService;

    @Test
    public void testAddProduct() throws AppException 
    {
        Product mockProduct = new Product();
        when(iProductService.addProduct(any())).thenReturn(1);

        ResponseEntity<?> responseEntity = productController.addProduct(mockProduct);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals("Product Added successfully", responseEntity.getBody());
    }

    @Test
    public void testUpdateProduct() throws AppException 
    {
        Product mockProduct = new Product();
        mockProduct.setId(1);
        when(iProductService.updateProductById(any())).thenReturn(mockProduct);

        ResponseEntity<?> responseEntity = productController.updateProduct(1, mockProduct);

        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        assertEquals(mockProduct, responseEntity.getBody());
    }

    @Test
    public void testDeleteProduct() throws AppException 
    {
        int productId = 1;
        when(iProductService.deleteProductById(productId)).thenReturn("Product deleted successfully");

        ResponseEntity<?> responseEntity = productController.deleteProduct(productId);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals("Product deleted successfully", responseEntity.getBody());
    }
}

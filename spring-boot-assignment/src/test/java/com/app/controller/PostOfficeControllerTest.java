package com.app.controller;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.app.pojos.PostOfficeResponseBean;
import com.app.service.IPostService;

@RunWith(MockitoJUnitRunner.class)
public class PostOfficeControllerTest 
{

	@Mock
	IPostService iPostService;
	
	@InjectMocks
	PostOfficeController postOfficeController;
	
	@Test
	public void testGetPostOfficeByCity() 
	{
		String city ="pune";
		PostOfficeResponseBean mockedResponse = new PostOfficeResponseBean();
		mockedResponse.setMessage("success");
		
		when(iPostService.fetchPostOfficeDetailsByCity(city)).thenReturn(mockedResponse);
		
		PostOfficeResponseBean actualResponse = postOfficeController.getPostOfficeByCity(city);
		assertEquals(mockedResponse, actualResponse);
		assertEquals("success",actualResponse.getMessage());
		
	}

}

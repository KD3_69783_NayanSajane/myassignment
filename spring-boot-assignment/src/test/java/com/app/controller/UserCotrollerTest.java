package com.app.controller;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.app.dto.UserDTO;
import com.app.exception.AppException;
import com.app.pojos.Product;
import com.app.pojos.Users;
import com.app.service.IProductService;
import com.app.service.IUserService;

@RunWith(MockitoJUnitRunner.class)
public class UserCotrollerTest 
{
	@InjectMocks
	UserCotroller userController;
;	
	@Mock
	IProductService iProductService;
	
	@Mock
	IUserService iUserService;

	@Test
	public void testGetProductById() 
	{
		Product mockProduct = new Product();
		int productId =1;
		when(iProductService.findProductByid(productId)).thenReturn(mockProduct);
		
		ResponseEntity<?> responseEntity = userController.getProductById(productId);
		
		assertEquals(mockProduct, responseEntity.getBody());
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
	}
	
	@Test
	public void testGetAllProducts()
	{
		List<Product> mockProductList = new ArrayList<>();
		mockProductList.add(new Product(1,"A52s", 67000.00f, "Red", 128, 4, "Samsung", 4.5f));
		when(iProductService.findAllProdustList()).thenReturn(mockProductList);
		
		ResponseEntity<?> allProducts = userController.getAllProducts();
		assertEquals(mockProductList, allProducts.getBody());
		assertEquals(1, mockProductList.size());
		assertEquals(HttpStatus.OK, allProducts.getStatusCode());
	}
	
	@Test
	public void testGetAllProductsByJpa()
	{
		List<Product> mockProductList = new ArrayList<>();
		mockProductList.add(new Product(1,"A52s", 67000.00f, "Red", 128, 4, "Samsung", 4.5f));
		mockProductList.add(new Product(1,"A52s", 67000.00f, "Red", 128, 4, "Samsung", 4.5f));
		when(iProductService.getAllProductList(1, 100, "id", "asc")).thenReturn(mockProductList);
		
		ResponseEntity<?> responseEntity = userController.getAllProductsByJpa(1, 100, "id", "asc");
		assertEquals(mockProductList, responseEntity.getBody());
		assertEquals(2, mockProductList.size());
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());	
	}
	
	@Test
	public void testLoginUser() throws AppException
	{
		Users mockUser = new Users();
		UserDTO mockUserDTO = new UserDTO();
		String email = "test@example.com";
	    String password = "password123";
	    mockUser.setEmail(email);
	    mockUser.setPassword(password);
	    mockUserDTO.setEmail(email);
	    
	    when(iUserService.authenticateUser(email, password)).thenReturn(mockUserDTO);
	    
	    ResponseEntity<?> loginUser = userController.loginUser(mockUser);
	    assertEquals(HttpStatus.OK, loginUser.getStatusCode());
	    assertEquals(mockUserDTO, loginUser.getBody());
	    assertEquals("test@example.com", mockUserDTO.getEmail());
	}
	
	@Test
	public void testUserRegistration() throws AppException
	{
		Users mockedUser = new Users();
        mockedUser.setEmail("test@example.com");
        mockedUser.setPassword("password");

        when(iUserService.registerUser(mockedUser)).thenReturn(mockedUser);

        ResponseEntity<?> responseEntity = userController.userRegistration(mockedUser);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals("Registration is successful. Now Login", responseEntity.getBody());
	}
}

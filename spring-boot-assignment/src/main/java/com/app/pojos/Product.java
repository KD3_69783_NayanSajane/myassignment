package com.app.pojos;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="product")
@JsonIgnoreProperties({"users"})
public class Product 
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(length = 20, nullable = false)
	@NotBlank(message = "Product Name should be entered!!")
	private String productName;
	
	@Column(nullable = false)
	private float cost;
	
	@Column(nullable = false, length = 10)
	private String color;
	
	@Column(nullable = false)
	private int storage;
	
	@Column(nullable = false)
	private int ram;
	
	@Column(nullable = false, length = 10)
	private String operator;
	
	@Column(nullable = false)
	private float rating;
	
	@ManyToMany(cascade = {CascadeType.MERGE, CascadeType.PERSIST}, 
				mappedBy = "products", fetch = FetchType.EAGER)
	private List<Users> users = new ArrayList<>();

	
	
	public Product() 
	{
	}

	public Product(int id, String productName, float cost, String color, int storage, int ram, String operator, float rating) 
	{
		super();
		this.id = id;
		this.productName = productName;
		this.cost = cost;
		this.color = color;
		this.storage = storage;
		this.ram = ram;
		this.operator = operator;
		this.rating = rating;
	}



	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public String getProductName() {
		return productName;
	}



	public void setProductName(String productName) {
		this.productName = productName;
	}



	public float getCost() {
		return cost;
	}



	public void setCost(float cost) {
		this.cost = cost;
	}



	public String getColor() {
		return color;
	}



	public void setColor(String color) {
		this.color = color;
	}



	public int getStorage() {
		return storage;
	}



	public void setStorage(int storage) {
		this.storage = storage;
	}



	public int getRam() {
		return ram;
	}



	public void setRam(int ram) {
		this.ram = ram;
	}



	public String getOperator() {
		return operator;
	}



	public void setOperator(String operator) {
		this.operator = operator;
	}



	public float getRating() {
		return rating;
	}



	public void setRating(float rating) {
		this.rating = rating;
	}



	public List<Users> getUsers() {
		return users;
	}



	public void setUsers(List<Users> users) {
		this.users = users;
	}



	@Override
	public String toString() {
		return "Product [id=" + id + ", productName=" + productName + ", cost=" + cost + ", color=" + color
				+ ", storage=" + storage + ", ram=" + ram + ", operator=" + operator + ", rating=" + rating + "]";
	}
	
	
}

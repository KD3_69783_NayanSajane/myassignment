package com.app.service;

import java.util.List;

import com.app.dto.UserDTO;
import com.app.exception.AppException;
import com.app.pojos.Users;

public interface IUserService 
{
	Users registerUser(Users user) throws AppException;
	UserDTO authenticateUser(String email, String password) throws AppException;
	List<Users> getAllUsers();
}

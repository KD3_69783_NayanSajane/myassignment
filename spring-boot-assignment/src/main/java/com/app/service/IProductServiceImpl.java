package com.app.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.app.config.SpringJdbcConfig;
import com.app.dao.ProductDao;
import com.app.exception.AppException;
import com.app.pojos.Product;
import com.app.repo.ProductRepo;

import org.springframework.data.domain.Sort;

@Service
@Transactional
public class IProductServiceImpl implements IProductService 
{
	@Autowired
	ProductRepo productRepo;
	
	@Autowired
	ProductDao productDao;

	@Override
	public int addProduct(Product p) throws AppException 
	{
		System.out.println("Inside service add layer...");
		//ApplicationContext context = new ClassPathXmlApplicationContext("com/app/config/config.xml");
//		ApplicationContext context = new AnnotationConfigApplicationContext(SpringJdbcConfig.class);
//		ProductDao productD = context.getBean(ProductDao.class);
		
		int result = productDao.saveProductByJdbcTamplate(p);
		return result;
	}

	@Override
	public Product updateProductById(Product newDetails) throws AppException 
	{
		System.out.println("Inside service for Update ...");
		//ApplicationContext context = new ClassPathXmlApplicationContext("com/app/config/config.xml");
//		ApplicationContext context = new AnnotationConfigApplicationContext(SpringJdbcConfig.class);
//		ProductDao productD = context.getBean(ProductDao.class);
		
		int result = productDao.updateProductByJdbcTemplate(newDetails);
		return newDetails;
	}

	@Override
	public String deleteProductById(int id) throws AppException
	{
		System.out.println("Inside service delete ...");
		//ApplicationContext context = new ClassPathXmlApplicationContext("com/app/config/config.xml");
//		ApplicationContext context = new AnnotationConfigApplicationContext(SpringJdbcConfig.class);
//		ProductDao productD = context.getBean(ProductDao.class);
		
		int result = productDao.deleteProductByJdbcTemplate(id);
		if(result==1)
			return "Product with id " +id + " deleted successsfully!!!";
		else
			return "Product not exist !!";
	}

	@Override
	public Product findProductByid(int id) 
	{
		System.out.println("Inside service get product by Id ...");
		//ApplicationContext context = new ClassPathXmlApplicationContext("com/app/config/config.xml");
//		ApplicationContext context = new AnnotationConfigApplicationContext(SpringJdbcConfig.class);
//		ProductDao productD = context.getBean(ProductDao.class);
		
		return productDao.getProductById(id);
		//return product;
	}

	@Override
	public List<Product> findAllProdustList() 
	{
//		System.out.println("Inside service get product list ...");
//		//ApplicationContext context = new ClassPathXmlApplicationContext("com/app/config/config.xml");
//		ApplicationContext context = new AnnotationConfigApplicationContext(SpringJdbcConfig.class);
//		ProductDao productD = context.getBean(ProductDao.class);
//		return productD.getAllProductList();
		
		List<Product> demoList = new ArrayList<>();
		for(int i=1; i<=1000; i++) 
		{
			Product product = new Product();
			demoList.add(product);
		}
		
		List<MyThreads> MyThreadsList = new ArrayList<>();
		for(int i=1; i<=1000; i++) 
		{
			MyThreads t1 = new MyThreads();
			t1.start();
			MyThreadsList.add(t1);
		}
		
		
		List<Product> list = new ArrayList<>();
		productDao.getAllProductList().forEach(p ->{
		Product product = new Product();
		BeanUtils.copyProperties(p, product);
		list.add(product);
		});
		return list;
	}

	@Override
	public List<Product> getAllProductList(Integer pageNumber, Integer pageSize, String sortBy, String sortDir) 
	{
		Sort sort = null;
		if (sortDir.equalsIgnoreCase("asc"))
			sort = Sort.by(sortBy).ascending();
		else
			sort = Sort.by(sortBy).descending();
		
		//Pageable pd = PageRequest.of(pageNumber, pageSize, Sort.by(sortBy));
		
		Pageable pd = PageRequest.of(pageNumber, pageSize, sort);
		
		Page<Product> pageProduct = this.productRepo.findAll(pd); // returning not all pages but returning requested page of products
		List<Product> productList = pageProduct.getContent();   // will return the list of products from that page.
		
		
		return productList;
	}
	
}

package com.app.service;

import com.app.pojos.PostOfficeResponseBean;


public interface IPostService 
{
	
	public PostOfficeResponseBean fetchPostOfficeDetailsByCity(String city);
	
	//public PostOfficeResponseBean fetchPostOfficeDetailsByCity(String city, int pageNumber, int pageSize);
}

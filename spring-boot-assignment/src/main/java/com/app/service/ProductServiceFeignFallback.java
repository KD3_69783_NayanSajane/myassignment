package com.app.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.app.pojos.PostOfficeResponseBean;

@Component
public class ProductServiceFeignFallback implements IProductServiceFeign 
{
	private static final Logger logger = LoggerFactory.getLogger(ProductServiceFeignFallback.class);
	
	@Override
    public PostOfficeResponseBean[] fetchPostOfficeDetailsFeignByCity(String city)
	{
        logger.error("Feign client call to productService failed for city: " + city);
        
        PostOfficeResponseBean[] responseBeanArray = new PostOfficeResponseBean[1];
        PostOfficeResponseBean errorResponse = new PostOfficeResponseBean();
        errorResponse.setMessage("server down");
        responseBeanArray[0] = errorResponse;
        return responseBeanArray;
    }

}

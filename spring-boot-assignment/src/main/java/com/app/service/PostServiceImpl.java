package com.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.app.pojos.PostOfficeResponseBean;

@Service
public class PostServiceImpl implements IPostService 
{

	@Autowired
	RestTemplate restTemplate;
	
	@Autowired
	IProductServiceFeign iProductServiceFeign;
	
	@Override
	public PostOfficeResponseBean fetchPostOfficeDetailsByCity(String cityName) 
	{
//		String url = "https://api.postalpincode.in/postoffice/{city}";
//		url = url.replace("{city}", cityName);
//		System.out.println("URL = " + url);
		
		//PostOfficeResponseBean[] responseBeanArray = restTemplate.getForObject(url, PostOfficeResponseBean[].class);
		
		PostOfficeResponseBean[] responseBeanArray = iProductServiceFeign.fetchPostOfficeDetailsFeignByCity(cityName);

	    if (responseBeanArray != null && responseBeanArray.length > 0) 
	        return responseBeanArray[0];
	    else 
	        return null;
	}	
	
	
}

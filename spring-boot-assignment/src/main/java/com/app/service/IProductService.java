package com.app.service;

import java.util.List;
import com.app.exception.AppException;
import com.app.pojos.Product;

public interface IProductService 
{
	int addProduct(Product product) throws AppException;

	Product updateProductById(Product newDetails) throws AppException;

	String deleteProductById(int id) throws AppException;

	Product findProductByid(int id);

	List<Product> findAllProdustList();

	List<Product> getAllProductList(Integer pageNumber, Integer pageSize, String sortBy, String sortDir);

}

package com.app.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.app.pojos.PostOfficeResponseBean;

@FeignClient(value = "productService", url = "${postoffice.service.base-url}", fallback = ProductServiceFeignFallback.class)
public interface IProductServiceFeign 
{

	@GetMapping("/{city}")
	PostOfficeResponseBean[] fetchPostOfficeDetailsFeignByCity(@PathVariable String city); 
	
//	@GetMapping("/{city}")
//	PostOfficeResponseBean[] fetchPostOfficeDetailsFeignByCity(@PathVariable String city, 
//			@RequestParam int pageNumber, @RequestParam int pageSize); 
}

// http://localhost:7070/post/{city}
package com.app.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.dao.UserDao;
import com.app.dto.UserDTO;
import com.app.exception.AppException;
import com.app.pojos.Users;

@Service
@Transactional
public class IUserServiceImpl implements IUserService 
{
	@Autowired
	UserDao userDao;
	
	@Override
	public Users registerUser(Users user) throws AppException 
	{
		System.out.println("Inside user register service");
		String email = user.getEmail();
		Users isExist = userDao.findByEmail(email);
		System.out.println(user);
		if(isExist == null)
		{
			return userDao.save(user);
		}
		else
		{
			throw new AppException("Email already Exist");
		}
	}

	@Override
	public UserDTO authenticateUser(String email, String password) throws AppException 
	{
		System.out.println("Inside authenticate User service");
		Users isAvailable = null;
		UserDTO userDTO = null;
		if(email !=null && password !=null)
			isAvailable = userDao.findByEmailAndPassword(email, password);
		else
			throw new AppException("Credentials can't be empty");
		
		if(isAvailable != null)
		{
			if(isAvailable.getPassword().equals(password))
			{
				userDTO = new UserDTO();
				BeanUtils.copyProperties(isAvailable, userDTO, "password");
			}
			else
				throw new AppException("Invalid Details");
		}
		else
			throw new AppException("No such user exist");
		return userDTO;
	}

	@Override
	public List<Users> getAllUsers() 
	{
		System.out.println("Inside get all user service");
		List<Users> userList = new ArrayList<>();
		userDao.findAll().forEach(u -> {
		Users user = new Users();
		BeanUtils.copyProperties(u, user);
		userList.add(user);
		});
		return userList;
	}

	
}

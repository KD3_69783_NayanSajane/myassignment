package com.app.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.app.exception.AppException;
import com.app.pojos.Product;

@Repository
public class ProductDao 
{
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
//	public JdbcTemplate getJdbcTemplate() {
//		return jdbcTemplate;
//	}
//
//	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
//		this.jdbcTemplate = jdbcTemplate;
//	}

	public int saveProductByJdbcTamplate(Product p) throws AppException
	{
		System.out.println("Inside save Dao layer...");
		
//		String query = "insert into product(product_name, cost, color, storage, ram, operator, rating) "
//				+ "values('"+p.getProductName()+"', '"+p.getCost()+"', '"+p.getColor()+"', "
//						+ "'"+p.getStorage()+"', '"+p.getRam()+"', '"+p.getOperator()+"', '"+p.getRating()+"')";
//		
//		int result = this.jdbcTemplate.update(query);
		
		String query = "insert into product(product_name, cost, color, storage, ram, operator, rating) values(?,?,?,?,?,?,?)";
		int result = this.jdbcTemplate.update(query, p.getProductName(), p.getCost(), p.getColor(), p.getStorage(),
				p.getRam(), p.getOperator(), p.getRating());
		System.out.println("Product Added..");
		return result;
		
	}

	public int updateProductByJdbcTemplate(Product p) 
	{
		System.out.println("Inside Dao layer for update...");
		
		String query = "update product set product_name=?, cost=?, color=?, storage=?, ram=?, operator=?, rating=? where id=?";
		
		int result = this.jdbcTemplate.update(query, p.getProductName(), p.getCost(), p.getColor(), p.getStorage(), p.getRam(),
					p.getOperator(), p.getRating(), p.getId());
		System.out.println("Product Updated ...");
		return result;
		
		
	}

	public int deleteProductByJdbcTemplate(int id) 
	{
		System.out.println("Inside Dao layer for Delete...");
		String query = "delete from product where id=?";
		int result = this.jdbcTemplate.update(query, id);
		return result;
	}
	
	public Product getProductById(int id)
	{
		System.out.println("Inside get product by Id : Dao ");
		String query = "select * from product where id =?";
		RowMapper<Product> rowMapper = new RowMapperImpl();
		
		try {
			return this.jdbcTemplate.queryForObject(query, rowMapper, id);  //  to retrieve a single row from the result set.
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
	}
	
	public List<Product> getAllProductList()
	{
		String query = "select * from product";
		try {
			return this.jdbcTemplate.query(query, new RowMapperImpl()); //to retrieve multiple rows from the result set.
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
	}

}

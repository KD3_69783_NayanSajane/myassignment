package com.app.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.pojos.Users;

@Repository
public interface UserDao extends JpaRepository<Users, Integer> 
{
	Users findByEmail(String email);
	Users findByEmailAndPassword(String email, String password);
}

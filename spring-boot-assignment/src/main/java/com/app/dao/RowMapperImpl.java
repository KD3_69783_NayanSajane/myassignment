package com.app.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.app.pojos.Product;

public class RowMapperImpl implements RowMapper<Product> 
{

	@Override
	public Product mapRow(ResultSet rs, int rowNum) throws SQLException 
	{
		Product product = new Product();
		product.setId(rs.getInt(1));
		product.setColor(rs.getString(2));
		product.setCost(rs.getFloat(3));
		product.setOperator(rs.getString(4));
		product.setProductName(rs.getString(5));
		product.setRam(rs.getInt(6));
		product.setRating(rs.getFloat(7));
		product.setStorage(rs.getInt(8));
		
		return product;
	}
	
}

package com.app.config;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

@Configuration
@ComponentScan(basePackages = {"com.app.dao"}) // to tell in which path dao is located.
public class SpringJdbcConfig 
{
	@Bean(name = {"ds"})
	public DataSource mysqlDataSource()
	{
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
		dataSource.setUrl("jdbc:mysql://localhost:3306/serviceDB");  // protocal:subprotocol
		dataSource.setUsername("root");
		dataSource.setPassword("Nayan#123");
		
		return dataSource; 
	}
	
	@Bean("jdbcTemplate")
	public JdbcTemplate getTemplate() 
	{
		JdbcTemplate jdbcTemplate = new JdbcTemplate();
		jdbcTemplate.setDataSource(mysqlDataSource());
		return jdbcTemplate;
	}
		
}

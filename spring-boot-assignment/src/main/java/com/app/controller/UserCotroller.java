package com.app.controller;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.app.dto.UserDTO;
import com.app.exception.AppException;
import com.app.pojos.Product;
import com.app.pojos.Users;
import com.app.service.IProductService;
import com.app.service.IUserService;

@RestController
@CrossOrigin
@RequestMapping("/user")
public class UserCotroller 
{
	private static final Logger logger = LoggerFactory.getLogger(ProductController.class);
	
	@Autowired
	IProductService iProductService;
	
	@Autowired
	IUserService iUserService;
	
	@GetMapping("/allproducts/{id}")
	public ResponseEntity<?> getProductById(@PathVariable int id)
	{
		Product product = iProductService.findProductByid(id);	
		if(product == null)
			return new ResponseEntity<>("No product is available with id " +id, HttpStatus.NO_CONTENT);
		else	
			return new ResponseEntity<>(product, HttpStatus.OK);
	}
	
	@GetMapping("/allproducts")
	public ResponseEntity<?> getAllProducts()
	{
		List<Product> list = null;
		list = iProductService.findAllProdustList();
		
		if(list.isEmpty())
			return new ResponseEntity<>("No products are there !!", HttpStatus.NO_CONTENT);
		else
			return new ResponseEntity<>(list, HttpStatus.OK);
	}
	
	@GetMapping("/getproducts")
	public ResponseEntity<?> getAllProductsByJpa(
			@RequestParam(value = "pageNumber", defaultValue = "0", required = false) Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = "100", required = false) Integer pageSize,
			@RequestParam(value = "sortBy", defaultValue = "id", required = false) String sortBy,
			@RequestParam(value = "sortDir", defaultValue = "asc", required = false) String sortDir)
	{
		List<Product> list = null;
		list = iProductService.getAllProductList(pageNumber, pageSize, sortBy, sortDir);
		
		if(list.isEmpty())
			return new ResponseEntity<>("No product found !!", HttpStatus.NO_CONTENT);
		else
			return new ResponseEntity<>(list, HttpStatus.OK);
	}
	
	@PostMapping("/login")
	public ResponseEntity<?> loginUser(@RequestBody Users user) throws AppException
	{
		try
		{
			logger.info("user details"+ user);
			String email = user.getEmail();
			String password = user.getPassword();
			UserDTO userDTO = iUserService.authenticateUser(email, password);
			
			if(userDTO != null)
				return new ResponseEntity<>(userDTO, HttpStatus.OK);
			else
			{
				logger.error("Something went wrong!!");
				throw new AppException("Something went wrong");
			}
		}
		catch(RuntimeException e)
		{
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PostMapping("/register")
	public ResponseEntity<?> userRegistration(@RequestBody Users user) throws AppException
	{
		System.out.println(user);
		logger.info("user details"+ user);
		try {
			String password = user.getPassword();
			user.setPassword(password);
			Users u = iUserService.registerUser(user);
			
			if(u != null)
				return new ResponseEntity<>("Registration is successful. Now Login", HttpStatus.OK);
			else
			{
				logger.error("Invalid Customer Registration Data!!");
				throw new AppException("Invalid Customer Registration Data!!");
			}
		}
		catch(RuntimeException e)
		{
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
}

package com.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.app.pojos.PostOfficeResponseBean;
import com.app.service.IPostService;

@RestController
@CrossOrigin
@RequestMapping("/postal")
public class PostOfficeController 
{
	@Autowired
	IPostService iPostService;
	
	@GetMapping("/byCity")
	public PostOfficeResponseBean getPostOfficeByCity(@RequestParam String city)
	{
		System.out.println("before calling Service");
		PostOfficeResponseBean postResponse = iPostService.fetchPostOfficeDetailsByCity(city);
		System.out.println("After calling service");
		return postResponse;
	}
	
	
}

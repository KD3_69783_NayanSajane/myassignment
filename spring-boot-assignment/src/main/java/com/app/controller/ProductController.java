package com.app.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.exception.AppException;
import com.app.pojos.Product;
import com.app.service.IProductService;

@RestController
@CrossOrigin
@RequestMapping("/product")
public class ProductController 
{
	private static final Logger logger = LoggerFactory.getLogger(ProductController.class);
	
	@Autowired
	IProductService iProductService;
	
	@PostMapping("/add")
	public ResponseEntity<?> addProduct(@RequestBody Product prodDetails) throws AppException
	{
		logger.info("Product details : " + prodDetails);
		int result = iProductService.addProduct(prodDetails);
		
		try {
			return new ResponseEntity<>("Product Added successfully", HttpStatus.OK);
		} catch (RuntimeException e) {
			return new ResponseEntity<>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		//return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@PutMapping("/update/{id}")
	public ResponseEntity<?> updateProduct(@PathVariable int id, @RequestBody Product newDetails) throws AppException
	{
		newDetails.setId(id);
		System.out.println("new id " + newDetails.getId());
		Product newProduct = iProductService.updateProductById(newDetails);
		
		try {
			return new ResponseEntity<>(newProduct, HttpStatus.CREATED);
		} catch (RuntimeException e) {
			return new ResponseEntity<>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<?> deleteProduct(@PathVariable int id) throws AppException
	{
		String message = iProductService.deleteProductById(id);
		try {
			return new ResponseEntity<>(message, HttpStatus.OK);
		} catch (RuntimeException e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
		}
		
	}
}

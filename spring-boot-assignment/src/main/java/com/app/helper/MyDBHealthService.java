package com.app.helper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;

@Controller
public class MyDBHealthService implements HealthIndicator 
{
	@Autowired
	private JdbcTemplate jdbcTemplate;
	 
    @Override
    public Health health() 
    {
        int errorCode = check();
        if (errorCode != 0) 
        {
            return Health.down().withDetail("Error Code", errorCode).build();
        }
        return Health.up().withDetail("Success Code", errorCode).build();
    }

    private int check() 
    { 
    	try 
    	{
	        jdbcTemplate.queryForObject("SELECT 1", Integer.class);
	        return 0; 
    	} 
    	catch (Exception e) 
    	{
    		return 1;  
    	}
    }
}

package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import com.example.controller.CalciController;
import com.example.service.CalculatorService;
import com.example.wsimport.Calculator;
import com.example.wsimport.CalculatorSoap;

@SpringBootApplication
//@ComponentScan(basePackageClasses= {CalciController.class,CalculatorService.class,CalculatorSoap.class})
//@ComponentScan(basePackages = "com.example.wsimport")
//@ComponentScan({ "com.example.wsimport,com.example.controller,com.example.service"})
public class CalciDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(CalciDemoApplication.class, args);
		
		
	}
	
}

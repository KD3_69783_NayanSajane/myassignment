package com.example.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ws.client.core.WebServiceTemplate;

import com.example.wsimport.CalculatorSoap;

@Service
public class CalculatorService 
{
	 private final WebServiceTemplate webServiceTemplate;

	    @Autowired
	    public CalculatorService(WebServiceTemplate webServiceTemplate) {
	        this.webServiceTemplate = webServiceTemplate;
	    }

	    public int add(int a, int b) {
	        // Create request payload directly without using a generated template
	        String requestPayload = "<Add xmlns=\"http://tempuri.org/\"><intA>" + a + "</intA><intB>" + b + "</intB></Add>";

	        // Send request and receive response
	        String responsePayload = (String) webServiceTemplate.marshalSendAndReceive(requestPayload);

	        // Parse the response
	        // Note: Modify this part based on your actual response structure
	        int result = Integer.parseInt(responsePayload);

	        return result;
	    }
}

package com.example.wsimport;

public class CalculatorClient 
{
    public static void main(String[] args) 
    {
    
        Calculator calculator = new Calculator();
        CalculatorSoap calculatorSoap = calculator.getCalculatorSoap();
        
        int resultAdd = calculatorSoap.add(5, 20);
        int resultSub = calculatorSoap.subtract(20, 5);
        int resultMulti = calculatorSoap.multiply(20, 5);
        int resultDevide = calculatorSoap.divide(20, 5);
        
        System.out.println("Result of add     " + resultAdd);
        System.out.println("Result of subtract" + resultSub);
        System.out.println("Result of multiply" + resultMulti);
        System.out.println("Result of divide  " + resultDevide);
    }
}


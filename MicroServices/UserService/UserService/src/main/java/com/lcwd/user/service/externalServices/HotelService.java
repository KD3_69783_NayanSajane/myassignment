package com.lcwd.user.service.externalServices;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.lcwd.user.service.pojo.Hotel;

@FeignClient(name="HOTEL-SERVICE")
public interface HotelService 
{
	
	@GetMapping("/hotels/{hotelId}")
	Hotel getHotel(@PathVariable Integer hotelId);
}

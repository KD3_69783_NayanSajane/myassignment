package com.lcwd.user.service.pojo;

import lombok.Data;

@Data
public class Hotel 
{
	private Integer id;
	private String name;
	private String location;
	private String about;
}

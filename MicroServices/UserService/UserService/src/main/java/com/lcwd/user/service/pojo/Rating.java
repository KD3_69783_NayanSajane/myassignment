package com.lcwd.user.service.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder  // used in test case
public class Rating 
{
	private Integer ratingId;
	private Integer userId;
	private Integer hotelId;
	private int rating;
	private String feedback;
	
	private Hotel hotel;
}

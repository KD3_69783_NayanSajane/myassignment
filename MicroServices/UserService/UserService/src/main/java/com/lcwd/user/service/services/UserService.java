package com.lcwd.user.service.services;

import java.util.List;

import com.lcwd.user.service.pojo.Users;

public interface UserService 
{
	Users saveUser(Users user);
	
	List<Users> getAllUsers();
	
	Users getUser(Integer userId);
}

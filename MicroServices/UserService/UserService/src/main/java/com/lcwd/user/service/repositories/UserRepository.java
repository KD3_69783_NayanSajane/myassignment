package com.lcwd.user.service.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.lcwd.user.service.pojo.Users;

@Transactional
public interface UserRepository extends JpaRepository<Users, Integer> 
{

}

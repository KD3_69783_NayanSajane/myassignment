package com.lcwd.user.service.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lcwd.user.service.pojo.Users;
import com.lcwd.user.service.services.UserService;

@RestController
@RequestMapping("/users")
public class UserController 
{
	@Autowired
	UserService userService;
	
	@PostMapping("/addUser")
	public ResponseEntity<Users> createUser(@RequestBody Users user)
	{
		System.out.println(user);
		Users savedUser = userService.saveUser(user);
		return new ResponseEntity<>(savedUser, HttpStatus.CREATED); 
	}
	
	@GetMapping("/{userId}")
	public ResponseEntity<Users> getSingleUser(@PathVariable Integer userId)
	{
		Users user = userService.getUser(userId);
		return new ResponseEntity<>(user,HttpStatus.OK);
	}
	
	@GetMapping("/getAllUsers")
	public ResponseEntity<List<Users>> getAllUsers()
	{
		List<Users> alluser = userService.getAllUsers();
		//return ResponseEntity.ok(alluser);
		return new ResponseEntity<>(alluser, HttpStatus.OK);
	}
	
}

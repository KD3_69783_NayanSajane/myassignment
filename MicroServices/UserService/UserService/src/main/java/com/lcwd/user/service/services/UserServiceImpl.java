package com.lcwd.user.service.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.lcwd.user.service.exceptions.ResourceNotFoundException;
import com.lcwd.user.service.externalServices.HotelService;
import com.lcwd.user.service.externalServices.RatingService;
import com.lcwd.user.service.pojo.Hotel;
import com.lcwd.user.service.pojo.Rating;
import com.lcwd.user.service.pojo.Users;
import com.lcwd.user.service.repositories.UserRepository;

@Service
public class UserServiceImpl implements UserService 
{

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Autowired
	private HotelService hotelService;
	
	@Autowired
	private RatingService ratingService;
	
	private Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);
	
	@Override
	public Users saveUser(Users user) 
	{
		return userRepository.save(user);
	}

//	//Rest template
//	@Override
//	public List<Users> getAllUsers() 
//	{
//		List<Users> allUsers = userRepository.findAll();
//		for (Users user : allUsers) 
//		{
//			List<Rating> ratingList = 
//					restTemplate.getForObject("http://RATING-SERVICE/ratings/users/"+user.getUserId(), ArrayList.class);
//			user.setRatings(ratingList);
//		}
//		
//		return allUsers;
//	}
	
	//Feign client
	@Override
	public List<Users> getAllUsers() 
	{
		List<Users> allUsers = userRepository.findAll();
		for (Users user : allUsers) 
		{
			List<Rating> ratingByUserId = ratingService.getRatingByUserId(user.getUserId());
			user.setRatings(ratingByUserId);
			
			for (Rating rating : ratingByUserId) 
			{
				Hotel hotel = hotelService.getHotel(rating.getHotelId());
				rating.setHotel(hotel);
			}
		}
		
		
		
		return allUsers;
	}
	
	// Feign client
	@Override
	public Users getUser(Integer userId) 
	{
		System.out.println("Inside user service");
		Users user = userRepository.findById(userId)
				.orElseThrow(()-> new ResourceNotFoundException("User not found " + userId));
		List<Rating> ratingList = ratingService.getRatingByUserId(userId);

		for (Rating rating : ratingList) 
		{
			
			Hotel hotel = hotelService.getHotel(rating.getHotelId());
			rating.setHotel(hotel);
		}
		user.setRatings(ratingList);
		return user;
	}

	// Map Method
//	@Override
//	public Users getUser(Integer userId) 
//	{
//		System.out.println("Inside user service");
//		Users user = userRepository.findById(userId)
//				.orElseThrow(()-> new ResourceNotFoundException("User not found " + userId));
//		Rating[] ratingArray = restTemplate.getForObject("http://RATING-SERVICE/ratings/users/"+userId, Rating[].class);
//		//user.setRatings(ratingList);
//		List<Rating> ratingList = Arrays.stream(ratingArray).toList();
//		logger.info("{} ", ratingList);
//		
//		List<Rating> ratingListWithHotel = ratingList.stream().map(rating->{
//			
//			ResponseEntity<Hotel> responseEntity = restTemplate.getForEntity("http://HOTEL-SERVICE/hotels/"+rating.getHotelId(), Hotel.class);
//			Hotel hotel = responseEntity.getBody();
//			logger.info("response status {} ", responseEntity.getStatusCode());
//			rating.setHotel(hotel);
//			return rating;
//		}).collect(Collectors.toList());
//		
//		user.setRatings(ratingListWithHotel);
//		return user;
//	}
	
	// For each 
//	@Override
//	public Users getUser(Integer userId) 
//	{
//		System.out.println("Inside user service");
//		Users user = userRepository.findById(userId)
//				.orElseThrow(()-> new ResourceNotFoundException("User not found " + userId));
//		Rating[] ratingArray = restTemplate.getForObject("http://RATING-SERVICE/ratings/users/"+userId, Rating[].class);
//		List<Rating> ratingList = Arrays.stream(ratingArray).toList();
//		//user.setRatings(ratingList);
//		
//		for (Rating rating : ratingList) 
//		{
//			ResponseEntity<Hotel> ratingListWithHotel = restTemplate.getForEntity("http://HOTEL-SERVICE/hotels/"+rating.getHotelId(), Hotel.class);
//			Hotel hotel = ratingListWithHotel.getBody();
//			rating.setHotel(hotel);
//		}
//		user.setRatings(ratingList);
//		return user;
//	}
	
}












package com.lcwd.user.service.externalServices;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.lcwd.user.service.pojo.Rating;

@Service // for test case
@FeignClient(name="RATING-SERVICE")
public interface RatingService 
{

	@GetMapping("/ratings/users/{userId}")
	List<Rating> getRatingByUserId(@PathVariable Integer userId);
	
	@PostMapping("/ratings/create")
	Rating createRating(Rating rating);
	
}

package com.lcwd.rating.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lcwd.rating.pojos.Rating;

public interface RatingRepository extends JpaRepository<Rating, Integer> 
{
	List<Rating> findByUserId(Integer userId);
	List<Rating> findByHotelId(Integer hotelId);
}

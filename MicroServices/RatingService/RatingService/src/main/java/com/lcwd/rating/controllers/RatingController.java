package com.lcwd.rating.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lcwd.rating.pojos.Rating;
import com.lcwd.rating.services.RatingService;

@RestController
@RequestMapping("/ratings")
public class RatingController 
{
	@Autowired
	private RatingService ratingService;
	
	@PostMapping("/create")
	public ResponseEntity<Rating> createRatings(@RequestBody Rating rating)
	{
		Rating createdRating = ratingService.createRating(rating); 
		return new ResponseEntity<>(createdRating, HttpStatus.CREATED);
	}
	
	@GetMapping("/allRatings")
	public ResponseEntity<List<Rating>> getRatings()
	{
		List<Rating> ratingsList = ratingService.getRatings();
		return new ResponseEntity<>(ratingsList, HttpStatus.OK);
	}
	
	@GetMapping("/users/{userId}")
	public ResponseEntity<List<Rating>> getRatingByUserId(@PathVariable Integer userId)
	{
		List<Rating> ratingsList = ratingService.getRatingsByUserId(userId);
		return new ResponseEntity<>(ratingsList, HttpStatus.OK);
	}
	
	@GetMapping("/hotels/{hotelId}")
	public ResponseEntity<List<Rating>> getRatingByHotelId(@PathVariable Integer hotelId)
	{
		List<Rating> ratingsList = ratingService.getRatingsByHotelId(hotelId);
		return new ResponseEntity<>(ratingsList, HttpStatus.OK);
	}
}

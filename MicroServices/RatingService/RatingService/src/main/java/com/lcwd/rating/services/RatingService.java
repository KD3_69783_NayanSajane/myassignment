package com.lcwd.rating.services;

import java.util.List;

import com.lcwd.rating.pojos.Rating;

public interface RatingService 
{

	 Rating createRating(Rating rating);
	 List<Rating> getRatings();
	 List<Rating> getRatingsByUserId(Integer userId);
	 List<Rating> getRatingsByHotelId(Integer hotelId);
}
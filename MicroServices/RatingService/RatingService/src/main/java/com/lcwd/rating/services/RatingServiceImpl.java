package com.lcwd.rating.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.lcwd.rating.pojos.Rating;
import com.lcwd.rating.repositories.RatingRepository;

@Service
public class RatingServiceImpl implements RatingService 
{
	@Autowired
	private RatingRepository ratingRepository;
	
	@Override
	public Rating createRating(Rating rating) 
	{
		return ratingRepository.save(rating);
	}

	@Override
	public List<Rating> getRatings() 
	{
		return ratingRepository.findAll();
	}

	@Override
	public List<Rating> getRatingsByUserId(Integer userId)
	{
		return ratingRepository.findByUserId(userId);
	}
	

	@Override
	public List<Rating> getRatingsByHotelId(Integer hotelId) 
	{
		return ratingRepository.findByHotelId(hotelId);
	}

}

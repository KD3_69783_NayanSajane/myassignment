package com.lcwd.hotel.services;

import java.util.List;

import org.springframework.stereotype.Service;

import com.lcwd.hotel.pojos.Hotel;

@Service
public interface HotelService 
{
	Hotel create(Hotel hotel);
	
	List<Hotel> getAll();
	
	Hotel get(Integer id);
}

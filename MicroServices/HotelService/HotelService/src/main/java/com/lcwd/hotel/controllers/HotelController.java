package com.lcwd.hotel.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lcwd.hotel.pojos.Hotel;
import com.lcwd.hotel.services.HotelService;

@RestController
@RequestMapping("/hotels")
public class HotelController 
{
	@Autowired
	private HotelService hotelService;
	
	@PostMapping("/addHotel")
	public ResponseEntity<Hotel> createHotel(@RequestBody Hotel hotel)
	{
		System.out.println(hotel);
		Hotel createdHotel = hotelService.create(hotel);
		return new ResponseEntity<>(createdHotel, HttpStatus.CREATED);
	}
	
	@GetMapping("/{hotelId}")
	public ResponseEntity<Hotel> getSingleHotel(@PathVariable Integer hotelId)
	{
		Hotel hotel = hotelService.get(hotelId);
		return new ResponseEntity<>(hotel, HttpStatus.CREATED);
	}
	
	@GetMapping("/getHotels")
	public ResponseEntity<List<Hotel>> getAllHotels()
	{
		List<Hotel> allHotels = hotelService.getAll();
		return new ResponseEntity<>(allHotels, HttpStatus.OK);
	}
	
}

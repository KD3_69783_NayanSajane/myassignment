package com.lcwd.hotel.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lcwd.hotel.pojos.Hotel;

import jakarta.transaction.Transactional;

@Transactional
public interface HotelRepository extends JpaRepository<Hotel, Integer> {

}

package com.example.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ws.client.core.WebServiceTemplate;

import com.example.wsimport.AddResponse;
import com.example.wsimport.CalculatorSoap;

@Service
public class CalculatorService 
{
    
    @Autowired
    CalculatorSoap calculatorSoap;

    public int add(int a, int b) 
    {
        int addedValue =calculatorSoap.add(a, b);
        return addedValue;

    }
}
	

/**
 * Calculator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.example.axis1;

public interface Calculator extends javax.xml.rpc.Service {
    public java.lang.String getCalculatorHttpSoap11EndpointAddress();

    public com.example.axis1.CalculatorPortType getCalculatorHttpSoap11Endpoint() throws javax.xml.rpc.ServiceException;

    public com.example.axis1.CalculatorPortType getCalculatorHttpSoap11Endpoint(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
    public java.lang.String getCalculatorHttpsSoap11EndpointAddress();

    public com.example.axis1.CalculatorPortType getCalculatorHttpsSoap11Endpoint() throws javax.xml.rpc.ServiceException;

    public com.example.axis1.CalculatorPortType getCalculatorHttpsSoap11Endpoint(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}

/**
 * CalculatorPortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.example.axis1;

public interface CalculatorPortType extends java.rmi.Remote {
    public java.lang.Integer add(java.lang.Integer n1, java.lang.Integer n2) throws java.rmi.RemoteException;
}

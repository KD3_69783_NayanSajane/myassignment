package com.example.axis1;

public class CalculatorPortTypeProxy implements com.example.axis1.CalculatorPortType {
  private String _endpoint = null;
  private com.example.axis1.CalculatorPortType calculatorPortType = null;
  
  public CalculatorPortTypeProxy() {
    _initCalculatorPortTypeProxy();
  }
  
  public CalculatorPortTypeProxy(String endpoint) {
    _endpoint = endpoint;
    _initCalculatorPortTypeProxy();
  }
  
  private void _initCalculatorPortTypeProxy() {
    try {
      calculatorPortType = (new com.example.axis1.CalculatorLocator()).getCalculatorHttpSoap11Endpoint();
      if (calculatorPortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)calculatorPortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)calculatorPortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (calculatorPortType != null)
      ((javax.xml.rpc.Stub)calculatorPortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.example.axis1.CalculatorPortType getCalculatorPortType() {
    if (calculatorPortType == null)
      _initCalculatorPortTypeProxy();
    return calculatorPortType;
  }
  
  public java.lang.Integer add(java.lang.Integer n1, java.lang.Integer n2) throws java.rmi.RemoteException{
    if (calculatorPortType == null)
      _initCalculatorPortTypeProxy();
    return calculatorPortType.add(n1, n2);
  }
  
  
}
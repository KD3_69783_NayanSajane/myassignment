package com.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.service.CalculatorService;

@RestController
@RequestMapping("/calci")
public class CalculatorController 
{
	@Autowired
	private CalculatorService calculatorService;

    @GetMapping("/add")
    public int add(@RequestParam int a, @RequestParam int b) 
    {
    	System.out.println(a + " " + b) ;
        return calculatorService.add(a, b);
    }
    
    @GetMapping("/hello")
    public String hello()
    {
    	return "hello nayan";
    }
}


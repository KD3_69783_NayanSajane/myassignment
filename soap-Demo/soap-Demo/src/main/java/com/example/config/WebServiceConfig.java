package com.example.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.client.core.WebServiceTemplate;

import com.example.wsimport.Calculator;
import com.example.wsimport.CalculatorSoap;

@Configuration
public class WebServiceConfig {
	
	@Bean
	public CalculatorSoap calculatorSoap()
	{
		 Calculator calculator = new Calculator();
	        CalculatorSoap calculatorSoap = calculator.getCalculatorSoap();
		return calculatorSoap();	
	}
	
	
}

package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "com.example.wsimport")
public class SoapDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SoapDemoApplication.class, args);
	}

}

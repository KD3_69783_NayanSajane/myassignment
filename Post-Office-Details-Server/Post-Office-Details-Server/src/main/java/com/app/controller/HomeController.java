package com.app.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("/home")
public class HomeController 
{
	
	@GetMapping("/get-data")
	public Map<String, String> getData()
	{
		Map<String, String> map = new HashMap<>();
		map.put("Nayan", "Sajane");
		
		return map;
		
	}
}

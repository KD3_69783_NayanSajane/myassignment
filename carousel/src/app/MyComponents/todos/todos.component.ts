import { Component, OnInit } from '@angular/core';
import { Todo } from "../../Todo";

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent implements OnInit {

  todos: Todo[]; // todos array of type Todo (TS syntax)

  constructor() {
    this.todos = [
      {
        sno: 1,
        title: "this is title 1",
        desc: "Descdd",
        active: true
      },
      {
        sno: 2,
        title: "this is title 2",
        desc: "Descdd",
        active: true
      },
      {
        sno: 3,
        title: "this is title 3",
        desc: "Descdd",
        active: true
      }
    ];
  }

  ngOnInit(): void {
    // Initialization logic if needed
  }
}

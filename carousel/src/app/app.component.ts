import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'To do list';

 constructor(){
  setTimeout(() => {
    this.title = "My name will change in 3sec";
  }, 3000);
 }
}
